/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import model.dao.PeixeDAO;
import model.model.Peixe;
import view.ViewDeAlgumCaralho;

/**
 *
 * @author aluno
 */
public class Controller implements ActionListener {

    ViewDeAlgumCaralho viewP = new ViewDeAlgumCaralho();
    PeixeDAO pDAO = new PeixeDAO();

    public Controller(ViewDeAlgumCaralho viewP, PeixeDAO pDAO) {
        this.pDAO = pDAO;
        this.viewP = viewP;
        this.viewP.btnAdd.addActionListener(this);
        this.viewP.btnEditar.addActionListener(this);
        this.viewP.btneditarOk.addActionListener(this);
        this.viewP.btnDeletar.addActionListener(this);
        this.viewP.btnListar.addActionListener(this);
        preencheTabela(viewP.JTpeixes);

    }

    public void preencheTabela(JTable JTPeixes) {
        DefaultTableModel modelo = new DefaultTableModel();
        JTPeixes.setModel(modelo);

        modelo.addColumn("IdPeixes");
        modelo.addColumn("Tipo");
        modelo.addColumn("TempoVida");
        Object[] coluna = new Object[3];

        int numRegistros = pDAO.listarPeixes().size();
        for (int i = 0; i < numRegistros; i++) {
            coluna[0] = pDAO.listarPeixes().get(i).getIdPeixe();
            coluna[1] = pDAO.listarPeixes().get(i).getTipo();
            coluna[2] = pDAO.listarPeixes().get(i).gettVida();
            modelo.addRow(coluna);
        }
    }
        public void preencheTabelaBuscar(String desc) {
        JTable JTPeixes = new JTable();
        DefaultTableModel modelo = new DefaultTableModel();
        JTPeixes.setModel(modelo);

        modelo.addColumn("IdPeixes");
        modelo.addColumn("Tipo");
        modelo.addColumn("TempoVida");
        Object[] coluna = new Object[3];

        int numRegistros = pDAO.listarPeixes().size();
        for (int i = 0; i < pDAO.buscarPeixe(desc).size(); i++) {
            coluna[0] = pDAO.listarPeixes().get(i).getIdPeixe();
            coluna[1] = pDAO.listarPeixes().get(i).getTipo();
            coluna[2] = pDAO.listarPeixes().get(i).gettVida();
            modelo.addRow(coluna);
                
        } 
      
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == viewP.btnAdd) {

            String tipo = viewP.txtTipo.getText();
            Integer tVida = (Integer.parseInt(viewP.txtVida.getValue().toString()));
            pDAO.insert(tipo, tVida);
            limparElementos();
            preencheTabela(viewP.JTpeixes);
        }
        if (e.getSource() == viewP.btnListar) {
            preencheTabela(viewP.JTpeixes);
        }
        if (e.getSource() == viewP.btnEditar) {
            int filaEditar = viewP.JTpeixes.getSelectedRow();
            int numFS = viewP.JTpeixes.getSelectedRowCount();
            if (filaEditar >= 0 && numFS == 1) {
                viewP.txtID.setText(String.valueOf((viewP.JTpeixes.getValueAt(filaEditar, 0))));
                viewP.btnAdd.setEnabled(false);
                viewP.btnEditar.setEnabled(false);
                viewP.btnDeletar.setEnabled(false);
            } else {
                JOptionPane.showMessageDialog(null, "Selecione uma linha!");
            }
        }
        if (e.getSource() == viewP.btnEditar) {
            int sRow = viewP.JTpeixes.getSelectedRow();
            int nRow = viewP.JTpeixes.getSelectedRowCount();
            if(sRow >=0 && nRow ==1){
                viewP.txtID.setText(String.valueOf((viewP.JTpeixes.getValueAt(sRow, 0))));
                viewP.txtTipo.setText(String.valueOf(viewP.JTpeixes.getValueAt(sRow, 1)));
                viewP.txtVida.setValue(viewP.JTpeixes.getValueAt(sRow, 2));
                viewP.btnAdd.setEnabled(false);
                viewP.btnDeletar.setEnabled(false);
                
            }else{
                JOptionPane.showMessageDialog(null, "Selecione uma linha!");
            }
        }
        
        if(e.getSource() == viewP.btneditarOk){
            int id = Integer.parseInt(viewP.txtID.getText());
            String tipo = viewP.txtTipo.getText();
            Integer vida = (Integer) viewP.txtVida.getValue();
            Peixe p = new Peixe();
            p.setTipo(tipo);
            p.settVida(vida);
            pDAO.update(id, p);
            
            limparElementos();
            viewP.btnAdd.setEnabled(true);
            viewP.btnEditar.setEnabled(true);
            viewP.btnDeletar.setEnabled(true);
//            viewP.btneditarOk.setEnabled(false);
        }

        if (e.getSource() == viewP.btnDeletar) {
            int filainicio = viewP.JTpeixes.getSelectedRow();
            Integer id;
             id = Integer.parseInt(viewP.JTpeixes.getValueAt(filainicio, 0).toString());
              pDAO.delete(id);
              preencheTabela(viewP.JTpeixes);
        }
            if (e.getSource() == viewP.btnBuscar) {
                preencheTabelaBuscar(viewP.txtBuscar.getText());

            }
        } 
      
     

    public void limparElementos() {
        viewP.txtID.setText("");
        viewP.txtTipo.setText("");
        viewP.txtVida.setValue(1);
    }

}
