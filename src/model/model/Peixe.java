package model.model;

public class Peixe {
    
    private Integer idPeixe;
    private String tipo;
    private Integer tVida;

    public Peixe() {
    }

    public Peixe(Integer idPeixe, String tipo, Integer tVida) {
        this.idPeixe = idPeixe;
        this.tipo = tipo;
        this.tVida = tVida;
    }

    public Integer getIdPeixe() {
        return idPeixe;
    }

    public void setIdPeixe(Integer idPeixe) {
        this.idPeixe = idPeixe;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public Integer gettVida() {
        return tVida;
    }

    public boolean settVida(Integer tVida) {
        if(tVida<1){
            return false;
        }
        this.tVida = tVida;
        return true;
    }
    
    
    
    
}
