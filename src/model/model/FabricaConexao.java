package model.model;

import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;

public class FabricaConexao {

    public String driver = "com.mysql.jdbc.Driver";
    public String server = "jdbc:mysql://localhost:3306/petshop";
    public String usuario = "root";
    public String senha = "root";

    public Connection getConnection() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection(server, usuario, senha);
        } catch (Exception e) {
            throw new RuntimeException("Erro!");
        }

    }
}
