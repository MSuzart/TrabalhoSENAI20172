package model.dao;

import com.mysql.jdbc.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.JOptionPane;
import model.model.FabricaConexao;
import model.model.Peixe;
import view.ViewDeAlgumCaralho;

public class PeixeDAO {

    FabricaConexao fc;

    public PeixeDAO() {
        fc = new FabricaConexao();
    }
    
    public void insert(String tipo, Integer tempoVida){
    	Connection con = (Connection) fc.getConnection();
    	PreparedStatement stmt = null;
    	
    	try{
    		stmt = con.prepareStatement("INSERT INTO PEIXE (TIPO, TEMPOVIDA)VALUES(?,?)");
    		stmt.setString(1, tipo);
    		stmt.setInt(2, tempoVida);
    		
    		stmt.executeUpdate();
    		JOptionPane.showMessageDialog(null, "Peixe foi inserido com sucesso");
    	}catch(SQLException ex){
    		JOptionPane.showMessageDialog(null, "Erro ao excluir");
    		
    	}
    }

    
     public void delete(Integer idPeixe){
    	Connection con = (Connection) fc.getConnection();
    	PreparedStatement stmt = null;
    	
    	try{
    		stmt = con.prepareStatement("DELETE FROM PEIXE WHERE IDPEIXE = ?");
    		stmt.setInt(1, idPeixe);
    		stmt.executeUpdate();
    		JOptionPane.showMessageDialog(null, "Peixe foi deletado");
    	}catch(SQLException ex){
    		JOptionPane.showMessageDialog(null, "Erro ao excluir");
    		
    	}
    }

     
     public void update(Integer id, Peixe p ){
    	Connection con = (Connection) fc.getConnection();
    	PreparedStatement stmt = null;
    	
    	try{
    		stmt = con.prepareStatement("UPDATE PRODUTO SET  TIPO = ?, TEMPOVIDA = ? WHERE IDPEIXE = ?");
    		stmt.setString(1, p.getTipo());
    		stmt.setInt(2, p.gettVida());
    		stmt.setInt(3, id);
    		stmt.executeUpdate();
    		JOptionPane.showMessageDialog(null, "Dados Atualizados");
    	}catch(SQLException ex){
    		JOptionPane.showMessageDialog(null, "Erro ao Atualizar");
    		
    	}
    }     
        
    
    public ArrayList<Peixe> listarPeixes(){
        ArrayList<Peixe> peixes = new ArrayList<Peixe>();
        try{   
        java.sql.Connection conexao;  
        conexao = fc.getConnection();
        String script = "SELECT * FROM PEIXE";
        PreparedStatement stmt = conexao.prepareStatement(script);
        ResultSet rs;
        rs = stmt.executeQuery();
        Peixe peixe;
        while(rs.next()){
            peixe = new Peixe();
            peixe.setIdPeixe(rs.getInt("idPeixe"));
            peixe.settVida(rs.getInt("tempoVida"));
            peixe.setTipo(rs.getString("tipo"));
            peixes.add(peixe);
        }
        stmt.close();
        }catch(Exception e){
            System.out.println("Erro!: "+e.getMessage());
        }
        return peixes;
        
    }
    
    
    public ArrayList<Peixe> buscarPeixe(String buscar){
         ArrayList<Peixe> peixes = new ArrayList<Peixe>();
         PreparedStatement stmt = null;
         ResultSet rs = null;
         java.sql.Connection conexao = fc.getConnection();
        try{   
        stmt = conexao.prepareStatement("SELECT * FROM PEIXE WHERE TIPO LIKE ?");
        stmt.setString(1, "%"+buscar+"%");
  
        rs = stmt.executeQuery();
        while(rs.next()){
           Peixe peixe = new Peixe();
            peixe.setIdPeixe(rs.getInt("idPeixe"));
            peixe.settVida(rs.getInt("tempoVida"));
            peixe.setTipo(rs.getString("tipo"));
            peixes.add(peixe);
        }
        stmt.close();
        }catch(Exception e){
            System.out.println("Erro!: "+e.getMessage());
        }
        return peixes;
    }
}
