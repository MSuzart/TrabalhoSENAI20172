/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeSupport;
import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Transient;

/**
 *
 * @author aluno
 */
@Entity
@Table(name = "peixe", catalog = "petshop", schema = "")
@NamedQueries({
    @NamedQuery(name = "Peixe.findAll", query = "SELECT p FROM Peixe p")
    , @NamedQuery(name = "Peixe.findByIdPeixe", query = "SELECT p FROM Peixe p WHERE p.idPeixe = :idPeixe")
    , @NamedQuery(name = "Peixe.findByTipo", query = "SELECT p FROM Peixe p WHERE p.tipo = :tipo")
    , @NamedQuery(name = "Peixe.findByTempoVida", query = "SELECT p FROM Peixe p WHERE p.tempoVida = :tempoVida")})
public class Peixe implements Serializable {

    @Transient
    private PropertyChangeSupport changeSupport = new PropertyChangeSupport(this);

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "idPeixe")
    private Integer idPeixe;
    @Column(name = "tipo")
    private String tipo;
    @Column(name = "tempoVida")
    private String tempoVida;

    public Peixe() {
    }

    public Peixe(Integer idPeixe) {
        this.idPeixe = idPeixe;
    }

    public Integer getIdPeixe() {
        return idPeixe;
    }

    public void setIdPeixe(Integer idPeixe) {
        Integer oldIdPeixe = this.idPeixe;
        this.idPeixe = idPeixe;
        changeSupport.firePropertyChange("idPeixe", oldIdPeixe, idPeixe);
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        String oldTipo = this.tipo;
        this.tipo = tipo;
        changeSupport.firePropertyChange("tipo", oldTipo, tipo);
    }

    public String getTempoVida() {
        return tempoVida;
    }

    public void setTempoVida(String tempoVida) {
        String oldTempoVida = this.tempoVida;
        this.tempoVida = tempoVida;
        changeSupport.firePropertyChange("tempoVida", oldTempoVida, tempoVida);
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idPeixe != null ? idPeixe.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Peixe)) {
            return false;
        }
        Peixe other = (Peixe) object;
        if ((this.idPeixe == null && other.idPeixe != null) || (this.idPeixe != null && !this.idPeixe.equals(other.idPeixe))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "view.Peixe[ idPeixe=" + idPeixe + " ]";
    }

    public void addPropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.addPropertyChangeListener(listener);
    }

    public void removePropertyChangeListener(PropertyChangeListener listener) {
        changeSupport.removePropertyChangeListener(listener);
    }
    
}
